package com.company.linesegment;

public class LineSegment {
    /*double x1;
    double y1;
    double x2;
    double y2;*/
    Point point1;
    Point point2;

    public LineSegment( Point point1, Point point2) {
        this.point1 = point1;
        this.point2 = point2;
    }
    public static boolean compare(LineSegment line1, LineSegment line2) {
        double firstLineLength = line1.length();
        double secondLineLength = line2.length();
        int comparisonResult = Double.compare(firstLineLength, secondLineLength);

        return comparisonResult == 0;
    }

    public double length() {
        return Math.sqrt(Math.pow(this.point1.x - this.point2.x, 2) + Math.pow(this.point1.y - this.point2.y, 2));
    }
}
