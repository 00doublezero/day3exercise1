package com.company.linesegment;

public class Main {

    public static void main(String[] args) {
	// write your code here

        Point point1 = new Point(0, 0);
        Point point2 = new Point(1, 1);
        Point point3 = new Point(-3, 0);
        Point point4 = new Point(1, 1);

        LineSegment line1 = new LineSegment(point1, point2);
        LineSegment line2 = new LineSegment(point3, point4);

        boolean comparison = LineSegment.compare(line1,line2);
        if (comparison) {
            System.out.println("Line segments is equals.");
        } else {
            System.out.println("Line segments is not equals.");
        }
    }
}
